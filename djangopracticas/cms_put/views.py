from django.shortcuts import render
from django.http import HttpResponse , Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt
def get_content(request, clave):
    if request.method == "PUT":
        valor = request.body.decode("utf-8")
        try:
            contenido = Contenido.objects.get(clave=clave)
            contenido.valor = valor
            contenido.save()
        except Contenido.DoesNotExist:
            new_contenido = Contenido(clave=clave, valor=valor)
            new_contenido.save()


    try:
        contenido = Contenido.objects.get(clave=clave)
        return HttpResponse(contenido.valor)
    except Contenido.DoesNotExist:
        return HttpResponse("No existe contenido para: " + clave)
